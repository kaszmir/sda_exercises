package pl.sdaacademy.exercises;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter //linijka 23-25
@Setter //linijka 27-29
@NoArgsConstructor //linijka 19-21
@AllArgsConstructor //linijka 15-17
class Dog {
    private String name;

//    public Dog(String name) {
//        this.name = name;
//    }
//
//    public Dog() {
//        this("bezimienny");
//    }
//
//    public String getName() {
//        return name;
//    }
//
//    public void setName(String name) {
//        this.name = name;
//    }


}
