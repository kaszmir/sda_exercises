package pl.sdaacademy.exercises;

public class Starter {

    public static void main(String[] args) {
        final Dog dog = new Dog();
        // TypObiektu nazwaObiektu = wywołanie konstruktora Typu (konstruktor ze wszystkimi parametrami)
        final Dog pimpek = new Dog("Pipek");
        pimpek.getName();
        pimpek.setName("Pimpus");

        double liczkaPrzecinkowa = 5.5;
        System.out.println(liczkaPrzecinkowa);
        int liczbaCalkowita = (int) liczkaPrzecinkowa;
        System.out.println(liczbaCalkowita);
        System.out.println((float)liczbaCalkowita);
        System.out.println(String.valueOf(liczbaCalkowita));
        String liczba = "100";
        System.out.println(Integer.parseInt(liczba));
    }

}
